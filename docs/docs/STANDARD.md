# Coding Standards

## Tips
* __a__: certainly __a__
* __[a]__: may be __a__
* __{a}__: certainly variable __a__
* __[a,b,c]__: __a__ or __b__ or __c__
* __{a,b,c}__: __a__ and __b__ and __c__

___________________
## Project and Source Directory Structure

> Project directory structure is fully standardized based on source language (Node.js, Erlang, C, Java, ...) and project type (server, web, library, ...) .
> So project directory structure is divided into two or more branches, A parent node contains `git configs` and `documents`, and the `source` directory that contains branches based on project branch source type (server, web, library, ...).

### project

> Base Project Directory Structure : `.git`, `docs`, `source (branched)`, `.gitignore`, `.gitmodules`, `.gitattributes`, `README.md`

* __{project_name}/__
    * __.git/__
        * ...
    * __docs/__
        * ...
    * __source/__
        * {type}\_[{description}]__
            * ...
        * ...
    * __.gitignore__
    * __.gitmodules__
    * __.gitattributes__   
    * __README.md__
    
* __types__:

    Name | Description
    ---|---    
    __node__ | Server application in Node.JS and Express.JS
    __erlang__ | Server application in Erlang and Misultin or Cowboy
    __c__ | Server or Library in C language
    __react__ | Web application in React and Webpack and Babel and Redux and Flux
    __qt__ | Cross Platform application in Qt and QML and JavaScript
    
### source_node
    
> Node.JS and Express.JS Projects Directory Structure : `node_modules`, `src`, `test`, `public`, `.npmrc`, `package.json`

* __node/__
    * __node_modules/__
        * ...   
    * __src/__
        * ...
    * __test/__
        * ...
    * __public/__
        * ...
    * __.npmrc__
    * __package.json__

### source_erlang
    
> Erlang and Misultin or Cowboy Projects Directory Structure : `deps`, `ebin`, `src`, `include`, `test`, `public`, `rebar.config`

* __erlang/__
    * __deps/__
        * ...   
    * __ebin/__
        * ...
    * __src/__
        * ...
    * __include/__
        * ...
    * __test/__
        * ...
    * __public/__
        * ...
    * __rebar.config__
    
### source_c
    
> C Library or Low level Server Projects Directory Structure : `libs`, `build`, `src`, `include`, `test`, `public`, `CMakeLists.txt`

* __c/__
    * __libs/__
        * ...   
    * __build/__
        * ...
    * __src/__
        * ...
    * __include/__
        * ...
    * __test/__
        * ...
    * __public/__
        * ...
    * __CMakeLists.txt__
    
### source_react
    
> React and Redux and Flux Projects Directory Structure : `node_modules`, `build`, `src`, `test`, `public`, `.npmrc`, `webpack.config.js`, `package.json`

* __react/__
    * __node_modules/__
        * ...   
    * __build/__
        * ...
    * __src/__
        * ...
    * __test/__
        * ...
    * __public/__
        * ...
    * __.npmrc__
    * __webpack.config.js__
    * __package.json__    

### source_qt
    
> Qt and QML and JavaScript Projects Directory Structure : `libs`, `build`, `src`, `include`, `test`, `public`, `{project}.pro`

* __qt/__
    * __libs/__
        * ...   
    * __build/__
        * ...
    * __src/__
        * ...
    * __include/__
        * ...
    * __test/__
        * ...
    * __public/__
        * ...
    * __{project}.pro__
    
    
### `src`, `include`, `test` Directory Structure

> `src` and `include` and `test` Directory Structure : `low`, `io`, `dbms`, `view`, `app.{format}`

* __[src,include,test]/__
    * __low/__
        * dsa/
        * process/
        * ...
    * __io/__
        * file/
            * ...
        * net/
            * http/
            * https/
            * tcp/
            * udp/
            * ws/
            * router/
            * ...
        * ...
    * __dbms/__
        * ...
    * __view/__
        * dialog_test.js
        * dialog_alert.js
        * dialog_bson.js
        * page_home.js
        * page_map.js
        * ...
    * __app.{format}__
___________________
## Source View(Component) Naming

> Any Source View(Component) Naming Standard

### Standards

* __{view_type}\_{description}__

### Examples

* node/
    * node_modules/
    * src/
        * low/
        * io/
        * dbms/
        * view/
            * __page_map.js__
    * test/
    * public/
    * package.json
___________________
## Source File(Module) Naming

> Any Source File(Module) Naming Standard

### Standards

* __{description}__

### Examples

* node/
    * node_modules/
    * src/
        * low/
        * io/
        * dbms/
            * __init.js__
            * __user.js__
            * __geo.js__
        * view/
    * test/
    * public/
    * package.json
___________________
## Source Class and Namespace Naming

> Any Class and Namespace Naming Standard

### Standards

* __{ModuleName}{Description}__

### Examples

* __app.js__:
``` 
class AppApplication {
    constructor() {...}
}
```    
_______________
## Source Method Naming

> Any Method Naming Standard

### Standards

* __{module_name}\_{description}__


### Examples

* __app.js__:    
``` 
class AppApplication {
    app_poller() {...}
}
```    
``` 
function app_poller() {...}
```
___________________
## Source Identifier Variable Naming

> Any Identifier (View bind) Variable Naming Standard

### Standards

* __{view_name}\_{component_type}\_{identifier_type}\_[{description}]__

### Examples

* __page_index.html__:
``` 
<body>
<div id="page_index_div_id_app" class="page_index_div_class_app">
...
</div>
</body>
```

* __page_main.qml__:
```
Window{
    id: page_main_window_id_app
    ...
}
```
___________________
## Source Global Variable Naming

> Any Global Variable Naming Standard

### Standards

* __{module_name}\_{description}__

### Examples

* __app.js__:
```
class AppApplication {
    constructor() {
        this.app_express = require('express');
    }
}
```
```
let app_express = require('express');
```
___________________
## Source Local Variable Naming

> Any Local Variable Naming Standard

### Standards

* __{description}__
* __{method_description}\_{description}__

### Examples

* __app.js__:
``` 
class AppApplication {
    app_poller(){
        for(let cursor = 0 ; ...) {
            ...
        }
    }
}
```
```
function app_poller(req, res) {
    let poller_alt = req.body.alt;
    ...
}
```
___________________
## Source Import Variable Naming

> Any Import Variable Naming Standard

### Standards

* __{description}__
* __{Description}__

### Examples

* __app.js__:
``` 
import Button from '../Button';
import Card from '../Card';
```
```
import {connect} from './interface';
```
___________________
## Source Commenting

> Any Commenting Standard

### Standards

* Before every blocks like `nested block`, `if..else`, `while...`, `do...while`, `function...`, `class...`.
* Before every DBMS Query.
* {block_type}: {Description}
* one line or multi line : `// loop: one line commenting`, `/* loop: line one   line two    line three */`
* block_types: `class`, `object`, `method`, `condition`, `loop`, `query`, `info`, etc

### Examples

* __app.js__:
``` 
// class: TGS application starter  
class AppApplication {
    
    // method: polling server function
    app_poller(){
        
        // for: iterate file descriptors for polling
        for(let app_poller_cursor = 0 ; ...) {
            ...
        }
    }
}
```
```
/* method:
app poller function 
async polling using epoller
*/
function app_poller(req, res) {
    let app_poller_alt = req.body.alt;
    ...
}
```
___________________
## Source Spacing

> Any Spacing Standard

### Standards

* After every `,` at args or params : `\space`.
* After every predefine of blocks : `\space`.
* After and Before every block : `\new line`.
* Before every comment : `\new line`.
* If method params count >= 5 then multi line params and args, else one line passing.
* MSI (Manual semicolon insertion) on ASI (Automatic semicolon insertion) languages like `javascript`.

### Examples

* __app.js__:
```
'use strict';   

class AppApplication {
    
    // method: poller loop
    app_poller (event_poll) {
    
        // while: wait loop
        while (true) {
            // if: break on new poll
            if (event_poll.isReady) {
                break;
            }
        }
                    
        // for: iterate file descriptors for polling
        for (let app_poller_cursor = 0 ; app_poller_cursor < 1000 ; app_poller_cursor++) {
            ...
        }
        
    }
    
}

```

