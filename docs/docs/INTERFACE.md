# Application Interface

## SQLson format

```json
{
    SELECT: {...},
    [WHERE]: {...},
    [GROUP]: {...},
    [HAVING]: {...},
    [ORDER]: {...},
    [LIMIT]: {...}
}
```

| Params               | Description                  |
| -------------------- | ---------------------------- |
| __SELECT__           | Select params and table      |
| __WHERE__            | Where conditions             |
| __GROUP__            | Group by                     |
| __HAVING__           | Having aggregators           |
| __ORDER__            | Order by                     |
| __LIMIT__            | Limit and offset             |

### SELECT

```json
SELECT: {
    ARRAY: [
        {
            COLUMN: "{column_name}",
            [AS]: "{as_name}"
        },
        {
            COLUMN: "{column_name}",
            [AS]: "{as_name}"
        },
        ...
    ],
    FROM: "{table_name}"
}
```

### WHERE

Where block types:

* __and__:

```json
{
    AND: [...]
}
```

*  __or__:

```json
{
    OR: [...]
}
```

*  __not__:

```json
{
    NOT: [...]
}
```

*  __condition__:

```json
{
    COLUMN: "{column_name}",
    OPERATOR: "[=,<>,>,<,>=,<=,BETWEEN,LIKE,IN]",
    [VALUE]: "{value_text}",
    [FROM]: "{from_text}",
    [TO]: "{to_text}",
    [PATTERN]: "{pattern_text}",
    [ARRAY]: [...],
    [SQLSON]: {...}
}
```

```json
WHERE: {
    ARRAY: [
        {
            COLUMN: "{column_name}",
            [AS]: "{as_name}"
        },
        {
            COLUMN: "{column_name}",
            [AS]: "{as_name}"
        },
        ...
    ],
    FROM: "{table_name}"
}
```

### GROUP

```json
GROUP: {
    ARRAY: [...]
}
```

### HAVING

Like where

### ORDER

```json
ORDER: {
    ARRAY: [...],
    [ORDER]: "[ASC,DESC]"
}
```

### LIMIT

```json
LIMIT: {
    LIMIT: {limit},
    [OFFSET]: {offset}
}
```

## Example

```json
{
    SELECT: {
        ARRAY: [
            {
                COLUMN: "column_1"
            },
            {
                COLUMN: "AVG(column_2)",
                AS: "avg"
            }
        ],
        FROM: "table"
    },
    WHERE: {
        AND: [
            {
                NOT: {
                    COLUMN: "column_1",
                    OPERATOR: ">",
                    VALUE: "10"
                }
            },
            {
                OR: [
                    {
                        COLUMN: "column_2",
                        OPERATOR: "LIKE",
                        PATTERN: "^test$"
                    },
                    {
                        COLUMN: "column_3",
                        OPERATOR: "BETWEEN",
                        FROM: "a",
                        TO: "ta"
                    },
                    {
                        COLUMN: "column_4",
                        OPERATOR: "IN",
                        ARRAY: ["a","b","c"]
                    },
                    {
                        COLUMN: "column_5",
                        OPERATOR: "IN",
                        SQLSON: {
                            SELECT: ...,
                            WHERE: ...
                        }
                    }
                ]
            }
        ]
    },
    GROUP: {
        ARRAY: ["column_1","column_2"]
    },
    HAVING: {
        ...like where
    },
    ORDER: {
        ARRAY: ["column_1","column_2"],
        ORDER: "ASC"
    },
    LIMIT: {
        LIMIT: 50,
        OFFSET: 10
    }
}
```