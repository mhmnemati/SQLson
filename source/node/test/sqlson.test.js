test('SQLson test', () => {
    expect.assertions(1);
    const { SQLson } = require('../src/app');
    expect(SQLson.sqlify(
        {
            SELECT: {
                ARRAY: [
                    {
                        COLUMN: 'col1',
                        AS: 'col1'
                    },
                    {
                        COLUMN: 'col2',
                        AS: 'col2'
                    },
                ],
                FROM: 'tbl'
            },
            WHERE: {
                AND: [
                    {
                        COLUMN: 'col1',
                        OPERATOR: '=',
                        VALUE: 'val1'
                    },
                    {
                        NOT: {
                            COLUMN: 'col2',
                            OPERATOR: 'LIKE',
                            PATTERN: 'pattern'
                        }
                    }
                ]
            }
        }
    )).toBe("SELECT col1 AS col1,col2 AS col2 FROM tbl WHERE ((col1='val1') AND (NOT (col2 LIKE 'pattern'))) ");
});